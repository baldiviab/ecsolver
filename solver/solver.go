package solver

import (
	"errors"
	"fmt"
)

var (
	ERR_TOO_MANY_PRECON = errors.New("Too many preconditions")
	ERR_PRECON_EXISTS   = errors.New("Precondition already exists in solution set")
	ERR_INVALID_PRECON  = errors.New("Problem Set returned invalid precondition value")
)

type Pset interface {
	ConstraintSize() int
	ConditionSize() int
	SolutionSize() int
	MinForUnique() int
	SolutionToCondition(int, int) (int, error)
	ConditionToSolution(int) (int, int, error)
	ConditionToConstraint(int) ([]int, error)
	ConstraintToCondition([]int) (int, error)
}

type Algorithm interface {
	Init(Pset) error
	SetPrecons([]int) error
	Solve() ([]int, int, error)
}

type Solver struct {
	Alg        Algorithm
	Ps         Pset
	Conditions []int
}

func NewSolver(p Pset, a Algorithm) (*Solver, error) {
	s := &Solver{
		Alg:        a,
		Ps:         p,
		Conditions: []int{},
	}
	fmt.Println("solver", s)

	err := s.Alg.Init(p)
	if err != nil {
		return &Solver{}, err
	}

	return s, nil
}

func (s *Solver) AddCondition(idx, val int) error {
	if len(s.Conditions) == s.Ps.SolutionSize() {
		return ERR_TOO_MANY_PRECON
	}
	cond, err := s.Ps.SolutionToCondition(idx, val)
	if err != nil {
		return err
	}
	for v := range s.Conditions {
		if cond == v {
			return ERR_PRECON_EXISTS
		}
	}
	s.Conditions = append(s.Conditions, cond)
	return nil
}

func (s *Solver) Solve() ([]int, int, error) {
	s.Alg.SetPrecons(s.Conditions)
	cond, cnt, err := s.Alg.Solve()
	return cond, cnt, err
}
