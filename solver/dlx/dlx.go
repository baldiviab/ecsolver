package dlx

import (
	"bufio"
	"errors"
	"fmt"
	"log"
	"os"
	"sort"
	"strconv"
	"strings"

	solver "bitbucket.org/baldiviab/ecsolver/solver"
)

var (
	SOLVED          = errors.New("Solved")
	REVERT          = errors.New("Revert")
	BRANCH_COMPLETE = errors.New("Finished searching decision branch")
	FOUND           = errors.New("Found Solution")
	EXISTS          = errors.New("Selection already exists")
	NOT_LAST        = errors.New("Unable to remove selection (not last selection made)")
)

type DLX struct {
	Rt *Root
}

type Root struct {
	C        map[int]*Head
	CR       *Head
	Solved   map[int]int
	Selected []int
	Ps       solver.Pset
}

type Head struct {
	Rt *Root
	N  int
	S  int
	O  *Dataobj
}

type Dataobj struct {
	C     *Head
	RowId int
	U     *Dataobj
	D     *Dataobj
	L     *Dataobj
	R     *Dataobj
}

func (a DLX) Init(p solver.Pset) error {
	a.Rt = newRoot()
	a.Rt.Ps = p

	// Initialize base incidence matrix

	a.Rt.Selected = make([]int, 0, a.Rt.Ps.SolutionSize())
	// Create Header Root
	a.Rt.CR = &Head{
		N: 1,
		S: 1,
		O: newNode(),
	}
	a.Rt.CR.Rt = a.Rt
	a.Rt.CR.O.C = a.Rt.CR
	fmt.Println(a.Rt.CR)
	fmt.Println(a.Rt.CR.O)
	// Create Constraint Column Headers
	for i := 0; i < a.Rt.Ps.ConstraintSize(); i++ {
		a.Rt.C[i] = &Head{
			N: i,
			S: 0,
			O: newNode(),
		}
		insertIntoRow(a.Rt.CR.O, a.Rt.C[i].O)
		a.Rt.C[i].O.C = a.Rt.C[i]
	}

	// Create Condition rows
	for i := 0; i < a.Rt.Ps.ConditionSize(); i++ {
		c, err := a.Rt.Ps.ConditionToConstraint(i)
		if err != nil {
			return err
		}
		r := newNode()
		r.RowId = i
		a.Rt.insertIntoCol(r, c[0])
		for j := 1; j < len(c); j++ {
			n := newNode()
			a.Rt.insertIntoCol(n, c[j])
			insertIntoRow(r, n)
		}
	}
	return nil
}

func (a DLX) SetPrecons(c []int) error {
	for i := 0; i < len(c); i++ {
		constraints, err := a.Rt.Ps.ConditionToConstraint(c[i])
		if err != nil {
			return err
		}
		Obj := a.Rt.C[constraints[0]].O.D
		for Obj != a.Rt.C[constraints[0]].O {
			if Obj.RowId == c[i] {
				Obj.selectRow()
				break
			}
		}
	}
	return nil
}

func (a DLX) Solve() ([]int, int, error) {
	fmt.Println(a)
	a.Rt.Dance()
	return []int{}, 0, nil
}

func newNode() *Dataobj {
	n := &Dataobj{}
	n.U = n
	n.D = n
	n.L = n
	n.R = n
	return n
}

func newRoot() *Root {
	return &Root{
		C:      make(map[int]*Head),
		CR:     &Head{},
		Solved: make(map[int]int),
	}

}

func insertIntoRow(r *Dataobj, i *Dataobj) {
	i.R = r
	i.L = r.L
	r.L.R = i
	r.L = i
}

func (r *Root) insertIntoCol(i *Dataobj, idx int) {
	i.C = r.C[idx]
	i.D = r.C[idx].O
	i.U = r.C[idx].O.U
	r.C[idx].O.U.D = i
	r.C[idx].O.U = i
	r.C[idx].S++
}

func (r *Root) getBoard() {
	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Enter board:\n")
	for i := 0; i < 9; i++ {
		text, err := reader.ReadString('\n')
		if err != nil {
			log.Fatal(err)
		}
		arr := strings.Split(text, " ")
		if len(arr) != 9 {
			log.Fatal("Invalid board invalid row length")
		}
		for j := 0; j < 9; j++ {
			v, err := strconv.Atoi(strings.Split(arr[j], "\n")[0])

			if err != nil {
				if strings.Split(arr[j], "\n")[0] == "_" {
					continue
				}
				log.Fatal(err)
			}
			if 0 < v && v < 10 {
				r.Solve(i, j, v-1)
			}
		}
	}
	return
}

func (r *Root) Solve(y, x, v int) {
	idx := (y * 9) + x
	r.Solved[idx] = v
}

func (r *Root) Unsolve(y, x, v int) {
	idx := (y * 9) + x
	delete(r.Solved, idx)
}

func (d *Dataobj) selectRow() error {
	d.coverCol()
	ref := d.R
	constraints := []int{d.C.N}
	for ref != d {
		err := ref.coverCol()
		if err != nil {
			return err
		}
		constraints = append(constraints, ref.C.N)
		ref = ref.R
	}
	condition, err := d.C.Rt.Ps.ConstraintToCondition(constraints)
	if err != nil {
		return err
	}
	d.C.Rt.addSelection(condition)
	return nil
}

func (r *Root) addSelection(c int) error {
	for _, v := range r.Selected {
		if v == c {
			return EXISTS
		}
	}
	r.Selected = append(r.Selected, c)
	return nil
}

func (r *Root) removeSelection(c int) error {
	if c != r.Selected[len(r.Selected)-1] {
		return NOT_LAST
	}
	r.Selected = r.Selected[:len(r.Selected)-1]
	return nil
}

func (d *Dataobj) unselectRow() error {
	ref := d.L
	for ref != d {
		ref.uncoverCol()
		ref = ref.L
	}
	d.uncoverCol()
	err := d.C.Rt.removeSelection(d.RowId)
	if err != nil {
		return err
	}
	return nil
}

func (d *Dataobj) uncoverCol() {
	ref := d.C.O.U
	// Uncover column header if covered
	if d.C.O.L.R != d.C.O {
		d.C.O.L.R = d.C.O
		d.C.O.R.L = d.C.O
	}
	var f bool
	for ref != d.C.O {
		if ref == d {
			f = false
		} else {
			f = true
		}
		ref.uncoverRow(f)
		if ref != d {
			d.C.S++
		}
		ref = ref.U
	}
}

func (d *Dataobj) uncoverRow(f bool) {
	ref := d.L
	for ref != d {
		ref.U.D = ref
		ref.D.U = ref
		if f {
			ref.C.S++
		}
		ref = ref.L
	}
}

func (d *Dataobj) coverCol() error {
	ref := d.C.O.D
	// Cover column header if not covered
	if d.C.O.L.R == d.C.O {
		d.C.O.L.R = d.C.O.R
		d.C.O.R.L = d.C.O.L
	}
	var i = 0
	var f bool
	for ref != d.C.O {
		if i == 20 {
			log.Fatalln("Fail")
		}
		if d == ref {
			f = false
		} else {
			f = true
		}
		err := ref.coverRow(f)
		if err != nil {
			return err
		}
		if ref != d {
			d.C.S--
		}
		ref = ref.D
		i++
	}
	return nil
}

func (d *Dataobj) coverRow(f bool) error {
	ref := d.R

	for ref != d {
		ref.U.D = ref.D
		ref.D.U = ref.U
		if f {
			ref.C.S--
		}
		ref = ref.R
	}
	return nil
}

func (r *Root) sScan() *Dataobj {
	ref := r.CR.O.R
	sel := r.CR.O.R
	for ref != r.CR.O {
		if ref.C.S < sel.C.S {
			sel = ref
		}
		ref = ref.R
	}
	return sel
}

func (r *Root) setSolved() {
	//fmt.Println("Solved:")
	a := []int{}
	for k, _ := range r.Solved {
		a = append(a, k)
	}
	sort.Ints(a)
	for _, v := range a {
		if v%9 == 0 {
			fmt.Printf("\n")
		}
		r.Solved[v]++
		fmt.Printf("%d ", r.Solved[v])
	}
	fmt.Printf("\n")
}

func (r *Root) Dance() error {
	fmt.Println("test")
	fmt.Println(r)
	if r.CR.O.R == r.CR.O {
		r.setSolved()
		return FOUND
	}
	sel := r.sScan()
	ref := sel.D
	for ref != sel {
		err := ref.selectRow()
		if err != nil {
			ref.unselectRow()
			ref = ref.D
			continue
		}
		err = r.Dance()
		if err == nil {
			return nil
		}
		ref.unselectRow()
		ref = ref.D
	}
	return BRANCH_COMPLETE
}
