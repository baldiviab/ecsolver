package sudoku

import (
	"errors"
)

type Sudoku9x9 struct{}

var (
	CONSTRAINTS             = 4
	SOLUTION_SIZE           = 9 * 9
	CONDITION_PER_POSITION  = 9
	MIN_PRECON_UNIQUE       = 17
	ERR_INVALID_CONSTRAINTS = errors.New("Invalid Constraints")
	ERR_INVALID_CONDITION   = errors.New("Invalid Condition")
)

func (s Sudoku9x9) ConstraintSize() int {
	return SOLUTION_SIZE * CONSTRAINTS
}

func (s Sudoku9x9) ConditionSize() int {
	return CONDITION_PER_POSITION * SOLUTION_SIZE
}

func (s Sudoku9x9) SolutionSize() int {
	return SOLUTION_SIZE
}

func (s Sudoku9x9) MinForUnique() int {
	return MIN_PRECON_UNIQUE
}

func (s Sudoku9x9) SolutionToCondition(idx, val int) (int, error) {
	condition := ((idx / 9) * SOLUTION_SIZE) + ((idx % 9) * CONDITION_PER_POSITION) + val

	return condition, nil
}

func (s Sudoku9x9) ConditionToSolution(c int) (int, int, error) {
	return ((c / SOLUTION_SIZE) * 9) + ((c % SOLUTION_SIZE) / CONDITION_PER_POSITION), c % CONDITION_PER_POSITION, nil
}

func (s Sudoku9x9) ConditionToConstraint(c int) ([]int, error) {

	return []int{
		(SOLUTION_SIZE * 0) + ((c / SOLUTION_SIZE) * 9) + (c % CONDITION_PER_POSITION),
		(SOLUTION_SIZE * 1) + ((c / SOLUTION_SIZE) * 9) + (c % CONDITION_PER_POSITION),
		(SOLUTION_SIZE * 2) + (((c / SOLUTION_SIZE) / 3) * 3) + ((c / SOLUTION_SIZE) / 3),
		(SOLUTION_SIZE * 3) + ((c / SOLUTION_SIZE) * 9) + (c / SOLUTION_SIZE),
	}, nil
}

func (s Sudoku9x9) ConstraintToCondition(c []int) (int, error) {
	if len(c) != CONSTRAINTS {
		return 0, ERR_INVALID_CONSTRAINTS
	}

	f := 0
	for i := 1; i < len(c); i++ {
		if c[f] > c[i] {
			f = i
		}
	}
	var x, y, v int

	for i := 0; i < len(c); i++ {
		if c[(f+i)%len(c)] < SOLUTION_SIZE*1 {
			y = c[(f+i)%len(c)] / 9
			v = c[(f+i)%len(c)] % 9
		} else if SOLUTION_SIZE*1 <= c[(f+i)%len(c)] && c[(f+i)%len(c)] < SOLUTION_SIZE*2 {
			x = c[(f+i)%len(c)] / 9
		}
	}

	condition := y*SOLUTION_SIZE + x*CONDITION_PER_POSITION + v

	return condition, nil
}
