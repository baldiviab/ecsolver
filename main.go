package main

import (
	"fmt"
	"time"

	ps "bitbucket.org/baldiviab/ecsolver/pset/9x9sudoku"
	solver "bitbucket.org/baldiviab/ecsolver/solver"
	alg "bitbucket.org/baldiviab/ecsolver/solver/dlx"
)

func main() {
	start := time.Now()
	p := ps.Sudoku9x9{}
	s, err := solver.NewSolver(ps.Sudoku9x9{}, alg.DLX{})
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println("Initialization Time: ", time.Since(start))
	setupBoardTime := time.Now()
	puz := "600008940900006100070040000200610000000000200089002000000060005000000030800001600"
	conditions := []int{}
	for i := 0; i < len(puz); i++ {
		if string(puz[i]) != "0" {
			cond, _ := p.SolutionToCondition(i, int(puz[i]))
			conditions = append(conditions, cond)
		}
	}
	fmt.Println("conds ", conditions)
	fmt.Println("Individual Puzzle Setup Time: ", time.Since(setupBoardTime))
	fmt.Println("main", s)
	s.Solve()
}
